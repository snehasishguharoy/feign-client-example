package com.example.microservice.currencyconversionservice.currencyconversionservice.svc;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservice.currencyconversionservice.currencyconversionservice.ifc.CurrencyExchangeServiceProxy;
import com.example.microservice.currencyconversionservice.currencyconversionservice.vo.CurrencyConversionBean;

@RestController
public class CurrencyConversionServiceSvc {
	@Autowired
	private CurrencyExchangeServiceProxy currencyExchangeServiceProxy;
	private static final Logger LOGGER=LoggerFactory.getLogger(CurrencyConversionServiceSvc.class);
	
	@GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrency(@PathVariable("from") String from,@PathVariable("to") String to,@PathVariable("quantity") BigDecimal quantity){
		CurrencyConversionBean conversionBean=currencyExchangeServiceProxy.retrieveExchangeValue(from, to);
		return new CurrencyConversionBean(conversionBean.getId(), conversionBean.getFrom(), conversionBean.getTo(), conversionBean.getConversionMultiple(),quantity,quantity.multiply(conversionBean.getConversionMultiple()), conversionBean.getPort());
		
	}

}
