package com.example.microservice.currencyexchange.currencyexchangeservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.microservice.currencyexchange.currencyexchangeservice.vo.ExchangeValue;
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {
	
	ExchangeValue findByFromAndTo(String from,String to);

}
