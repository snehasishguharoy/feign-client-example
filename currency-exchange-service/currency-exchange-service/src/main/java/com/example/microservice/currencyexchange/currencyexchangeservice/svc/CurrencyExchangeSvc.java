package com.example.microservice.currencyexchange.currencyexchangeservice.svc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservice.currencyexchange.currencyexchangeservice.dao.ExchangeValueRepository;
import com.example.microservice.currencyexchange.currencyexchangeservice.vo.ExchangeValue;

@RestController
public class CurrencyExchangeSvc {
	@Autowired
	private Environment environment;
	@Autowired
	private ExchangeValueRepository exchangeValueRepository;
	private static final Logger LOGGER=LoggerFactory.getLogger(CurrencyExchangeSvc.class);
	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public ExchangeValue retrieveExchangeValue(@PathVariable("from") String from,@PathVariable("to") String to){
		LOGGER.info("convertCurrency");
		ExchangeValue exchangeValue =exchangeValueRepository.findByFromAndTo(from, to);
		exchangeValue.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
		return exchangeValue;
	}

}
